#ifndef _TREE_H_
#define _TREE_H_

#include <stdlib.h>
#include <stdbool.h>

enum color {
    ROUGE='R', 
    NOIR='N'
};


typedef struct _TreeNode *Tree;

struct _TreeNode{
	Tree parent;
	Tree left;
    Tree right;
    char data[1];
    enum color couleur;
};

Tree tree_create(const void *data, size_t size);
Tree insertion(Tree racine, Tree arbre);
Tree insertion_auto(Tree racine, Tree arbre);
Tree update(Tree arbre);

bool rechercher(Tree racine, int data);
Tree suppression(Tree racine, int data);

Tree rotation_gauche(Tree racine);
Tree rotation_droite(Tree racine);

void tree_post_order(Tree arbre, void (*func) (void *, void *), void *extra_data);
void tree_post_order_couleur(Tree arbre);
#endif

