#include <string.h>
#include "tree-avl.h"
#include <stdbool.h>
#include "min-max.h"
#include <stdlib.h>
#include <stdio.h>

Tree tree_create (const void *data, size_t size){
  Tree tree = (Tree) malloc (3 * sizeof (Tree) + size);
  if(tree){
    tree->parent = NULL;
    tree->left = NULL;
    tree->right = NULL;
    tree->couleur = ROUGE;
    memcpy (tree->data, data, size);
  }
  return tree;
}

bool rechercher(Tree racine, int data){
  if(racine){
    if(*(int *)racine->data == data){
       return true;
    }
    if(data < *(int *)racine->data){
       return rechercher(racine->left, data);
    }else{
       return rechercher(racine->right, data);
    }
  }
  return false;
}

static void tree_delete(Tree arbre){
   if(arbre){
      tree_delete(arbre->left);
      tree_delete(arbre->right);

      //free(arbre->data);
      free(arbre);
   }
}


Tree suppression(Tree racine, int data){
   if(racine){
      if(*(int *)racine->data == data){
         //le noeud est feuille 
         if(racine->left == NULL && racine->right == NULL){
             Tree parent = racine->parent;
             if(parent->left == racine){
                 parent->left = NULL;
                 if(parent->right != NULL && parent->right->couleur == NOIR){
                    parent->right->couleur = ROUGE;
                 }
             }else{
                 parent->right = NULL;
                 if(parent->left != NULL && parent->left->couleur == NOIR){
                    parent->left->couleur = ROUGE;
                 }
             }
             tree_delete(racine);

             while(parent->parent != NULL){
		parent = parent->parent;
             }
             return parent;
         }
         //le noeud possède un fils à gauche 
         if(racine->left != NULL && racine->right == NULL){
	     Tree parent = racine->parent;
             if(parent->left == racine){
                 parent->left = racine->left;
                 racine->left->parent = parent; 
                 racine->left->couleur = racine->couleur;
                 racine->left = NULL;
             }else{
                 parent->right = racine->left;
                 racine->left->parent = parent;  
                 racine->left->couleur = racine->couleur;
                 racine->left = NULL;
             }
             tree_delete(racine);

             while(parent->parent != NULL){
		parent = parent->parent;
             }
             return parent;
         } 
         //le noeud possède un fils à droite 
         if(racine->right != NULL && racine->left == NULL){
             Tree parent = racine->parent;
             if(parent->left == racine){
                 parent->left = racine->right;
                 racine->right->parent = parent; 
                 racine->right->couleur = racine->couleur;
                 racine->right = NULL;
             }else{
                 parent->right = racine->right;
                 racine->right->parent = parent;  
                 racine->right->couleur = racine->couleur;
                 racine->right = NULL;
             }
             tree_delete(racine);

             while(parent->parent != NULL){
		parent = parent->parent;
             }
             return parent;
         }
      }
      if(data < *(int *)racine->data){
         return suppression(racine->left, data);
      }else{
         return suppression(racine->right, data);
      }
   }
   return NULL;
}

Tree insertion(Tree racine, Tree arbre){
  if(racine){
    bool ajout = false;
    Tree constant = racine;

    while(ajout == false){
      if((*(int *)arbre->data) < (*(int *)constant->data)){
        if(constant->left == NULL){
          constant->left = arbre;
          arbre->parent = constant;
          ajout = true;
        }
        constant = constant->left;
      }else{
        if(constant->right == NULL){
          constant->right = arbre;
          arbre->parent = constant;
          ajout = true;
        }
        constant = constant->right;
      }
    }
    return racine;

  }else{
    arbre->couleur = NOIR;
    return arbre;
  }
}

Tree insertion_auto(Tree racine, Tree arbre){
   if(racine){
    bool ajout = false;
    Tree constant = racine;

    while(ajout == false){
      if((*(int *)arbre->data) < (*(int *)constant->data)){
        if(constant->left == NULL){
          constant->left = arbre;
          arbre->parent = constant;
          ajout = true;

          if(constant->couleur == ROUGE){
	     //printf("le pere et le fils sont de couleur rouge");
             if(constant->parent->parent != NULL){
                //printf("il y a un arriere grand pere\n");
                Tree ar_gp = constant->parent->parent;
                if(ar_gp->right == constant->parent){
                   Tree u = update(arbre);
                   ar_gp->right = u;
                   u->parent = ar_gp; 
                }else{
                   Tree u = update(arbre);
                   ar_gp->left = u;
                   u->parent = ar_gp; 
                }

                while(ar_gp->parent != NULL){
                   ar_gp = ar_gp->parent;
                }
                return ar_gp;
             }else{
		racine = update(arbre);
                return racine;
	     }
          }
        }
        constant = constant->left;
      }else{
        if(constant->right == NULL){
          constant->right = arbre;
          arbre->parent = constant;
          ajout = true;

          if(constant->couleur == ROUGE){
	     //printf("le pere et le fils sont de couleur rouge");
             if(constant->parent->parent != NULL){
                //printf("il y a un arriere grand pere");
                Tree ar_gp = constant->parent->parent;
                if(ar_gp->right == constant->parent){
                   Tree u = update(arbre);
                   ar_gp->right = u;
                   u->parent = ar_gp; 
                }else{
                   Tree u = update(arbre);
                   ar_gp->left = u;
                   u->parent = ar_gp; 
                }

                while(ar_gp->parent != NULL){
                   ar_gp = ar_gp->parent;
                }
                return ar_gp;
             }else{
		racine = update(arbre);
                return racine;
	     }
          }
        }
        constant = constant->right;
      }
    }
    return racine;

  }else{
    arbre->couleur = NOIR;
    return arbre;
  }
}

Tree update(Tree arbre){
  if(arbre){
    //cas 1 : arbre = ROUGE, parent = ROUGE
    if(arbre->couleur == ROUGE && arbre->parent->couleur == ROUGE){
      Tree parent = arbre->parent;

      //cas 1.1 : parent->left = arbre
      if(parent->left == arbre){
        Tree grand_parent = parent->parent;
        bool racine = false;

        if(grand_parent->parent == NULL){
          racine = true;
        }

        if(grand_parent){
          //cas 1.1.1 : grand_parent->right = parent
          if(grand_parent->right != NULL && grand_parent->right == parent){
            grand_parent->right = rotation_droite(grand_parent->right);
            grand_parent = rotation_gauche(grand_parent);
            
            //mise à jour des couleurs 
            if(racine){
              grand_parent->parent = NULL;
              grand_parent->couleur = NOIR;
            }else{
              grand_parent->couleur = ROUGE;
            }

            grand_parent->left->couleur = NOIR;
            grand_parent->right->couleur = NOIR;

            return(grand_parent);
          }

          //cas 1.1.2 : grand_parent->left = parent 
          if(grand_parent->left != NULL && grand_parent->left == parent){
            grand_parent = rotation_droite(grand_parent);
            
            //mise à jour des couleurs 
            if(racine){
              grand_parent->parent = NULL;
              grand_parent->couleur = NOIR;
            }else{
              grand_parent->couleur = ROUGE;
            }

            grand_parent->left->couleur = NOIR;
            grand_parent->right->couleur = NOIR;

            return(grand_parent);
          }   
        }
      }

      //cas 1.2 : parent-> right = arbre 
      if(parent->right == arbre){
        Tree grand_parent = parent->parent;
        bool racine = false;

        if(grand_parent->parent == NULL){
          racine = true;
        }

        if(grand_parent){
          //cas 1.2.1 : grand_parent->right = parent
          if(grand_parent->right != NULL && grand_parent->right == parent){
            grand_parent = rotation_gauche(grand_parent);
            
            //mise à jour des couleurs 
            if(racine){
              grand_parent->parent = NULL;
              grand_parent->couleur = NOIR;
            }else{
              grand_parent->couleur = ROUGE;
            }

            grand_parent->left->couleur = NOIR;
            grand_parent->right->couleur = NOIR;

            return(grand_parent);
          }

          //cas 1.2.2 : grand_parent->left = parent 
          if(grand_parent->left != NULL && grand_parent->left == parent){
            grand_parent->left = rotation_gauche(grand_parent->left);
            grand_parent = rotation_droite(grand_parent);

            //mise à jour des couleurs 
            if(racine){
              grand_parent->parent = NULL;
              grand_parent->couleur = NOIR;
            }else{
              grand_parent->couleur = ROUGE;
            }

            grand_parent->left->couleur = NOIR;
            grand_parent->right->couleur = NOIR;

            return(grand_parent);
          }
        }
      }
    }
    return arbre;
  }
}



Tree rotation_gauche(Tree racine){
  Tree nouv_racine = racine->right;

  if(racine->parent != NULL){
    nouv_racine->parent = racine->parent;
  }else{
    nouv_racine->parent = NULL;
  }

  racine->right = nouv_racine->left;

  nouv_racine->left = racine;
  nouv_racine->left->parent = nouv_racine;

  return nouv_racine;
}

Tree rotation_droite(Tree racine){

  Tree nouv_racine = racine->left;
  if(racine->parent != NULL){
    nouv_racine->parent = racine->parent;
  }else{
    nouv_racine->parent = NULL;
  }

  racine->left = nouv_racine->right;

  nouv_racine->right = racine;
  nouv_racine->right->parent = nouv_racine;

  return nouv_racine;
}

void tree_post_order(Tree arbre, void (*func) (void *, void *), void *extra_data){
  if(arbre){
    tree_post_order(arbre->left, func, extra_data);
    tree_post_order(arbre->right, func, extra_data);
    func(arbre->data, extra_data);
  }
}

void tree_post_order_couleur(Tree arbre){
  if(arbre){
    tree_post_order_couleur(arbre->left);
    tree_post_order_couleur(arbre->right);
    printf("%c\n", (char)arbre->couleur);
  }
}

void tree_post_order_dico(Tree arbre, void (*print) (void *, FILE *), FILE * stream){
  if(arbre){
    tree_post_order_couleur(arbre->left);
    tree_post_order_couleur(arbre->right);
    print(arbre->data, stream);
  }
}


