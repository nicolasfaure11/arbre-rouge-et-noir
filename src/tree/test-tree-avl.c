#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include "tree-avl.h"

typedef struct {
    char *mot;
    char *def;
}Couple;

int mon_compare(const void *p1, const void *p2){
    return strcmp(((Couple *) p1)->mot, ((Couple *) p2)->mot);
}

void mon_print(void *data, FILE * stream){
    fprintf(stream, "%s:%s\n", ((Couple *) data)->mot, ((Couple *) data)->def);
}

void monPrintF (void * a, void * b){
    printf("Valeur du noeud : %d\n", *(int *)a);
}

/**
 * Test réalisés pour les arbres binaires
 * Affichage des résultats des tris
 */
void testArbresRN(void){
    printf("Test sur les arbres rouge et noir avec chiffre\n\n");
    int a = 10, b = 85, c = 15, d = 70, e = 20, f = 60, g = 30;
    size_t sizeInt = sizeof(int);

    Tree racine = tree_create(&a, sizeInt);
    Tree fils1 = tree_create(&b, sizeInt);
    Tree fils2 = tree_create(&c, sizeInt);

    insertion_auto(NULL, racine);
    insertion_auto(racine, fils1);
    racine = insertion_auto(racine, fils2);

    tree_post_order(racine, monPrintF, NULL);
    tree_post_order_couleur(racine);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    85                        N     N
    */

    printf("\n");

    Tree fils3 = tree_create(&d, sizeInt);
    
    racine = insertion_auto(racine, fils3);

    tree_post_order(racine, monPrintF, NULL);
    tree_post_order_couleur(racine);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    85                        N     N
                   /                              /
                  /                              /
                 70                             R
    */

    printf("\n");

    Tree fils4 = tree_create(&e, sizeInt);
    
    racine = insertion_auto(racine, fils4);

    tree_post_order(racine, monPrintF, NULL);
    tree_post_order_couleur(racine);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    70                        N     R
                   / \                            / \
                  /   \                          /   \
                20    85                        N     N
    */

    printf("\n");

    Tree fils5 = tree_create(&f, sizeInt);
    
    racine = insertion_auto(racine, fils5);

    tree_post_order(racine, monPrintF, NULL);
    tree_post_order_couleur(racine);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    70                        N     R
                   / \                            / \
                  /   \                          /   \
                20    85                        N     N
                 \				 \
                  \				  \
                  60                               R
    */

    printf("\n");

    Tree fils6 = tree_create(&g, sizeInt);

    racine = insertion_auto(racine, fils6);

    tree_post_order(racine, monPrintF, NULL);
    tree_post_order_couleur(racine);

        /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    70                        N     R
                   / \                            / \
                  /   \                          /   \
                30    85                        R     N
               / \			       / \   
              /   \			      /   \
             20   60                         N     N
    */

    printf("\nTest de la recherche : \n");
    printf("valeur 20 : %d\n", rechercher(racine, 20));
    printf("valeur 10 : %d\n", rechercher(racine, 10));
    printf("valeur 95 : %d\n", rechercher(racine, 95));

    printf("\nTest de la suppression : \n");
    printf("	Suppression d'une feuille : \n");
    
    Tree racine1 = tree_create(&a, sizeInt);
    Tree fils11 = tree_create(&b, sizeInt);
    Tree fils21 = tree_create(&c, sizeInt);

    insertion_auto(NULL, racine1);
    insertion_auto(racine1, fils11);
    racine1 = insertion_auto(racine1, fils21);

    tree_post_order(racine1, monPrintF, NULL);
    tree_post_order_couleur(racine1);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    85                        N     N
    */

    printf("\nSuppression de la valeur 85 : \n");

    suppression(racine1, 85);

    tree_post_order(racine1, monPrintF, NULL);
    tree_post_order_couleur(racine1);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /                               / 
              /                               /   
             10                              R     
    */

    printf("	Suppression d'un noeud qui possède un seul fils : \n");

    Tree racine2 = tree_create(&a, sizeInt);
    Tree fils12 = tree_create(&b, sizeInt);
    Tree fils22 = tree_create(&c, sizeInt);
    Tree fils32 = tree_create(&d, sizeInt);

    insertion_auto(NULL, racine2);
    insertion_auto(racine2, fils12);
    racine2 = insertion_auto(racine2, fils22);
    racine2 = insertion_auto(racine2, fils32);

    tree_post_order(racine2, monPrintF, NULL);
    tree_post_order_couleur(racine2);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    85                        N     N
                   /                              /
                  /                              /
                 70                             R
    */

    printf("\nSuppression de la valeur 85 : \n");

    racine2 = suppression(racine2, 85);

    tree_post_order(racine2, monPrintF, NULL);
    tree_post_order_couleur(racine2);

    /*
          valeur arbre                    couleur arbre
                15                              N
               /  \                            / \
              /    \                          /   \
             10    70                        N     N
    */
}

void testTempsExecution(void){
    double time_spend = 0.0;

    clock_t begin = clock();

    srand(time(NULL));

    int a = (rand() % (1000001 - 0)) + 0;;
    Tree racine = tree_create(&a, sizeof(int));
    insertion_auto(NULL, racine);

    for(int i = 1 ; i < 100000 ; i++){ 
        a = (rand() % (1000001 - 0)) + 0;;
    	Tree fils = tree_create(&a, sizeof(int));
        racine = insertion_auto(racine, fils);
    }
    
    clock_t end = clock();

    time_spend += (double)(end-begin) / CLOCKS_PER_SEC;
    printf("Temps execution : %f\n", time_spend);
}

int main(){
    testArbresRN();  
    return EXIT_SUCCESS;
}
